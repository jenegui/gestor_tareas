<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\models\{Task};
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MainController extends Controller
{
    /* Método para la visualizar la visa home */
    function home(Request $request){
        //Busca las tareas registradas muestra páginas de 20 registros
        $results=Task::paginate(20);
        $tipos=DB::table('tasks')->get();
        return view('home',['results'=>$results]);

        return view('home');
    }
    
    /* Método para visualizar la vista crear tarea */
    function register(){
        return view('create');
    }

    /* Método para visualizar la vista editar tarea */
    function editTask($id){
        //Busca el registro por id
        $result=Task::find($id);
        $tipos=DB::table('tasks')->get();
        return view('edit',['id'=>$id,'result'=>$result]);
    }

    /* Método para borrar tareas por id */
    function deletTask($id){
        //Boora el registro por id
        $result=Task::find($id);
        $result->delete();

        return redirect(url(''));
    }

    /* Método para registrar las tareas */
    function save_task(Request $request){
        $title = $request->input('title',"");
        $description = $request->input('description',"");
        $due_date = $request->input('due_date',"");
        
        //Realiza el insert
        $result=new Task();
        $result->title = $title;
        $result->description = $description;
        $result->due_date = $due_date;
        
        $result->save();

        return redirect(url(''));
    }

    /* Método para editar las tareas */
    function edit_task($id, Request $request){
        $title = $request->input('title',"");
        $description = $request->input('description',"");
        $due_date = $request->input('due_date',"");
        
        //Realiza el update
        $result=Task::find($id);
        $result->title = $title;
        $result->description = $description;
        $result->due_date = $due_date;
        
        $result->save();

        return redirect(url(''));
    }
   
    /* Método para obtener el listado de tareas vencidas o por hacer */
    public function getTasksByStatus($estatus)
    {
        // $estatus contiene el parámetro de la URL

        if ($estatus === 'vencidas') {
            $tasks = Task::where('due_date', '<', now())->get();
        } elseif ($estatus === 'porhacer') {
            $tasks = Task::where('due_date', '>=', now())->get();
        } else {
            $tasks = Task::all();
        }
       
        return response()->json(['tasks' => $tasks]);
    }

    public function vencidas()
    {
        // Realizar una solicitud GET a la ruta /due_tasks/vencidas de tu API
        $response = Http::get('http://127.0.0.1:8000/due_tasks/vencidas');

        // Obtener los datos de la respuesta en formato JSON
        $tasks = $response->json();

        // Hacer algo con los datos recibidos
        return view('reporte', ['tasks' => $tasks]);
    }

    public function porhacer()
    {
        // Realizar una solicitud GET a la ruta /due_tasks/porhacer de tu API
        $response = Http::get('http://127.0.0.1:8000/due_tasks/porhacer');

        // Obtener los datos de la respuesta en formato JSON
        $tasks = $response->json();

        // Hacer algo con los datos recibidos
        return view('reporte', ['tasks' => $tasks]);
    }
    
}
