@extends('layout')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Tarea</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('update', $result->id) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $result->title }}" required>
                        </div>

                        <div class="form-group">
                            <label for="description">Descripción</label>
                            <textarea class="form-control" id="description" name="description" rows="3">{{ $result->description }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="due_date">Fecha de Vencimiento</label>
                            <input type="date" class="form-control" id="due_date" name="due_date" value="{{ $result->due_date }}" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Actualizar Tarea</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
