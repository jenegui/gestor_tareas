@extends('layout')
<div class="text-center my-4">
    <a href="{{url('/registrar_tareas')}}"><button>Regitrar tareas</button></a>
    <a href="{{url('/vencidas')}}"><button>Tareas vencidas</button></a>
    <a href="{{url('/porhacer')}}"><button>Tareas por hacer</button></a>
</div>
<table class="table mt-4">
    <tr>
        <th>Título</th>
        <th>Descripción</th>
        <th>Fecha de vencimiento</th>
        <th>Editar</th>
        <th>Borrar</th>
    </tr>
    @forelse( $results as $result)
    <tr>
        <td>{{$result->title}}</td>
        <td>{{$result->description}}</td>
        <td>{{$result->due_date}}</td>
        <td>
            <a href="{{url('/editar_tareas/'.$result->id)}}"><button><i class="fa fa-pencil-alt"></i></button></a>
        </td>
        <td>
            <a href="{{url('/borrar_tareas/'.$result->id)}}"><button><i class="fa fa-trash-alt"></i></button></a>
        </td>
    </tr>
    @empty
    <tr>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>-</td>
    </tr>
    @endforelse    
</table>