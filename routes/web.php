<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta par las vistas de crear, editar y borrar registros
Route::get('/', [MainController::class, 'home']);
Route::get('/registrar_tareas', [MainController::class, 'register']);
Route::get('/editar_tareas/{id}', [MainController::class, 'editTask']);
Route::get('/borrar_tareas/{id}', [MainController::class, 'deletTask']);

//Rutas para guardar, editar y borrar registros
Route::post('/store', [MainController::class, 'save_task']);
Route::put('/update/{id}', [MainController::class, 'edit_task']);

//Ruta para las taras vendicas o por hacer
Route::get('/vencidas', [MainController::class, 'vencidas']);
Route::get('/porhacer', [MainController::class, 'porhacer']);

Route::get('/due_tasks/{vencidas}', [MainController::class, 'getTasksByStatus']);

